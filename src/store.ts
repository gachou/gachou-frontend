import Vue from 'vue';
import Vuex from 'vuex';
import { IGachouStatus, IMetadataEntry, ISearchResult } from '@/api';
import { client, ISearchParams } from './backend-client';
import { ACTIVATE_FILE, SEARCH, UPDATE_STATUS, DEACTIVATE_FILE, MOVE_TO_TRASH } from '@/store/actions';
import router from '@/router';

Vue.use(Vuex);

interface NumberMap {
  [name: string]: number;
}

interface StringMap {
  [name: string]: string;
}

export interface IFrontendState {
  backendStatus: IGachouStatus;
  lastError: string | null;
  search: ISearchResult | null;
  // The name of the active image
  activeFile: IMetadataEntry | null;
  filenameToIndex: NumberMap;
  recentlyDeletedFiles: string[];
}

const $store = new Vuex.Store<IFrontendState>({
  state: {
    lastError: null,
    backendStatus: {
      tasks: {
        running: 0,
        waiting: 0,
      },
    },
    search: null,
    activeFile: null,
    filenameToIndex: {},
    recentlyDeletedFiles: [],
  },
  mutations: {
    updateStatus(state: IFrontendState, status: IGachouStatus) {
      state.backendStatus = status;
    },
    updateLastError(state: IFrontendState, message: string) {
      state.lastError = message;
    },
    updateSearchResult(state: IFrontendState, searchResult: ISearchResult) {
      state.search = searchResult;
      state.filenameToIndex = {};
      state.search.results.forEach((value, index) => {
        state.filenameToIndex[value.name] = index;
      });
      if (state.activeFile && state.filenameToIndex[state.activeFile.name] == null) {
        state.activeFile = state.search.results[0];
      }
    },
    activateFile(state: IFrontendState, file: IMetadataEntry) {
      state.activeFile = file;
    },
    addDeletedFile(state: IFrontendState, fileName: string) {
      state.recentlyDeletedFiles.push(fileName);
    },
  },
  actions: {
    async [UPDATE_STATUS]({ commit }) {
      const status = await client.status();
      commit('updateStatus', status);
    },
    async [SEARCH]({ commit, state }, searchParams: Partial<ISearchParams>) {
      // Merge last search params and current serach params
      const lastSearchParams = getLastSearchParamsFromState(state);
      const newSearchParams: ISearchParams = {
        start: searchParams.start || lastSearchParams.start,
        limit: searchParams.limit || lastSearchParams.limit,
        facets: {
          ...lastSearchParams.facets,
          ...searchParams.facets,
        },
      };
      const result = await client.search(newSearchParams);
      commit('updateSearchResult', result);
    },
    async [ACTIVATE_FILE]({ commit }, name: string) {
      const result = await client.read(name);
      commit('activateFile', result);
    },
    async [DEACTIVATE_FILE]({ commit }) {
      commit('activateFile', undefined);
    },
    async [MOVE_TO_TRASH]({ commit, state }, name) {
      let currentIndex = 0;
      if (state.activeFile != null) {
        currentIndex = state.filenameToIndex[state.activeFile.name];
      }

      await client.moveToTrash(name);
      const lastSearchParams = getLastSearchParamsFromState(state);
      const result = await client.search(lastSearchParams);
      commit('addDeletedFile', name);
      commit('updateSearchResult', result);

      if (state.search != null) {
        const maxActiveFileIndex = state.search.results.length - 1;
        const nextActiveFileIndex = Math.min(currentIndex, maxActiveFileIndex);
        if (nextActiveFileIndex > -1) {
          const nextActiveFile = state.search.results[nextActiveFileIndex];
          router.push({ name: 'details', params: { name: nextActiveFile.name } });
        } else {
          router.push({ name: 'files' });
        }
      }
    },
  },
  getters: {
    fileRelativeToActiveFile: state => (steps: number): IMetadataEntry | null => {
      if (state.activeFile == null || state.search == null || state.search.results.length === 0) {
        return null;
      }
      const currentIndex = state.filenameToIndex[state.activeFile.name];
      // Restrict target index to range of search results array
      const targetIndex = Math.min(Math.max(0, currentIndex + steps), state.search.results.length - 1);
      return state.search.results[targetIndex];
    },
    pendingTasks: state => state.backendStatus.tasks.running + state.backendStatus.tasks.waiting,
  },
});

function getLastSearchParamsFromState(state: IFrontendState): ISearchParams {
  // Compute active facets from last search result
  const activeFacets: StringMap = {};
  if (state.search) {
    state.search.facets.forEach(facet => {
      facet.values.forEach(value => {
        if (value.active) {
          activeFacets[facet.id] = value.value;
        }
      });
    });
  }
  return {
    start: state.search ? state.search.context.start : 0,
    limit: state.search ? state.search.context.limit : 10,
    facets: activeFacets,
  };
}

export default $store;
