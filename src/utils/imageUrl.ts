import { IMetadataEntry } from '@/api';

export function imageUrl(file: IMetadataEntry, showVideosAsThumbnail: boolean, thumbSpec: string | null): string {
  if (!showVideosAsThumbnail && file.metadata.mimeType.match(/^video\/.*/)) {
    return `/backend/files/${file.name}`;
  }
  return thumbSpec != null ? `/backend/files/${file.name}?size=${thumbSpec}` : `/backend/files/${file.name}`;
}
