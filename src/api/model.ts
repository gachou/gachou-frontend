export interface IMetadata extends IWritableMetadata {
  mimeType: string;
  /**
   * Size of the video / image
   */
  extents: IExtents;
}

export interface IWritableMetadata {
  creationDate: Date;
  rating?: number;
}

/**
 * Dimensions of images and videos
 */
export interface IExtents {
  /**
   * Width in pixels (visible width)
   */
  width: number;
  /**
   * Height in pixels (visible height)
   */
  height: number;
  /**
   * Length (of a video) in milliseconds
   */
  lengthMs?: number;
}

/**
 * A string representing a scale specification (like "300x300")
 */
export type ScaleSpec = string;

/**
 * Interface containing status-information about the whole system
 */
export interface IGachouStatus {
  tasks: ITaskStatus;
}

export interface ITaskStatus {
  running: number;
  waiting: number;
}

export interface IMetadataEntry {
  /**
   * A unique name for this file
   */
  name: string;

  /**
   * The metadata of the file
   */
  metadata: IMetadata;
}

export interface IFacet {
  /**
   * The id of the facet and the query parameter in the search request
   */
  id: string;

  /**
   * The display name of the facet
   */
  label: string;

  /**
   * The possible values of this facet
   */
  values: IFacetValue[];
}

export interface IFacetValue {
  /**
   * The value of the query string parameter
   */
  value: string;

  /**
   * True, if this facet value is currently applied to the query
   */
  active: boolean;

  /**
   * Number of search results if this facet value is used in a query
   */
  count: number;
}

export interface ISearchContext {
  /**
   * Index of the first element that was requested
   */
  start: number;

  /**
   * Number of elements requested
   */
  limit: number;

  /**
   * Total number of elements found by the search
   */
  total: number;
}

export interface ISearchResult {
  facets: IFacet[];
  context: ISearchContext;
  results: IMetadataEntry[];
}
