import Vue from 'vue';
import Router from 'vue-router';
import About from './views/About.vue';
import Upload from './views/Upload.vue';
import Files from './views/Files.vue';
import Details from './views/Details.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/files',
    },
    {
      path: '/files',
      name: 'files',
      component: Files,
    },
    {
      path: '/details/:name',
      name: 'details',
      component: Details,
      props: true,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/upload',
      name: 'upload',
      component: Upload,
    },
  ],
});
