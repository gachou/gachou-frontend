import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPlayCircle,
  faPlusCircle,
  faArrowAltCircleLeft,
  faArrowAltCircleRight,
  faAngleRight,
  faAngleDoubleRight,
  faAngleDoubleLeft,
  faAngleLeft,
  faPauseCircle,
  faHome,
  faImages,
  faCog,
  faTasks,
  faSync,
  faCaretDown,
  faTrash,
  faDownload,
} from '@fortawesome/free-solid-svg-icons';

export const icons = {
  faPlusCircle,
  faPauseCircle,
  faPlayCircle,
  faAngleRight,
  faAngleDoubleRight,
  faAngleDoubleLeft,
  faAngleLeft,
  faArrowAltCircleLeft,
  faHome,
  faImages,
  faCog,
  faTasks,
  faArrowAltCircleRight,
  faSync,
  faCaretDown,
  faTrash,
  faDownload,
};

library.add(icons);
