export const SEARCH = 'search';
export const UPDATE_STATUS = 'updateStatus';
/**
 * Activate an image as main image in the view
 * @type {string}
 */
export const ACTIVATE_FILE = 'activateFile';

export const DEACTIVATE_FILE = 'deactivateFile';

export const MOVE_TO_TRASH = 'moveToTrash';
