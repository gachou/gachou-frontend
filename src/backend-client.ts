import axios, { AxiosResponse } from 'axios';
import { IGachouStatus, IMetadataEntry, ISearchResult } from '@/api';

export const API_BASE_URL = '/backend/';
const instance = axios.create({
  baseURL: API_BASE_URL,
  timeout: 5000,
});

interface StringMap {
  [key: string]: string;
}

export interface ISearchParams {
  start: number;
  limit: number;
  facets: StringMap;
}

export class Client {
  public async status(): Promise<IGachouStatus> {
    const status: AxiosResponse<IGachouStatus> = await instance.get<IGachouStatus>('status', { responseType: 'json' });
    return status.data;
  }

  public async search(searchParams: ISearchParams) {
    const searchResult = await instance.get<ISearchResult>('metadata', {
      params: {
        start: searchParams.start,
        limit: searchParams.limit,
        ...searchParams.facets,
      },
    });
    return searchResult.data;
  }

  public async read(name: string): Promise<IMetadataEntry> {
    const result = await instance.get<IMetadataEntry>(url`metadata/${name}`);
    return result.data;
  }

  public async moveToTrash(name: string): Promise<void> {
    await instance.delete(url`files/${name}`);
  }

  public async reindex(): Promise<void> {
    await instance.put<void>('metadata');
  }
}

export const client = new Client();

/**
 * URI-encode substiutions in the template literal
 * @param {TemplateStringsArray} template
 * @param substitutions
 * @return {string}
 */
function url(template: TemplateStringsArray, ...substitutions: any[]): string {
  return String.raw(template, substitutions.map(subst => encodeURIComponent(subst)));
}
