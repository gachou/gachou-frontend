declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module '@fortawesome/vue-fontawesome' {
  export const FontAwesomeIcon: any;
}

declare module 'vue2-touch' {
  import { PluginFunction, PluginObject } from 'vue';

  class Vue2TouchEvents implements PluginObject<{}> {
    [key: string]: any;

    public install: PluginFunction<{}>;
  }

  const VuePlugin: Vue2TouchEvents;

  export default VuePlugin;
}
