import { IMetadataEntry } from '@/api';

export interface ActiveElementEvent {
  newActiveFile: IMetadataEntry;
  boundingBox: {
    x: number;
    y: number;
    height: number;
    width: number;
  };
}
