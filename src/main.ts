import Vue from 'vue';
import App from './App.vue';
import Navigation from './components/Navigation.vue';
import Facets from './components/Facets.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import './scss/main.scss';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import BootstrapVue from 'bootstrap-vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Notifications from 'vue-notification';
import Vue2Touch from 'vue2-touch';
import Debug from 'debug';
import { icons } from './icons';
import { name, version } from './version';

const debug = Debug('gachou-frontend:main');

// tslint:disable-next-line:no-console
console.log('Gachou Frontend ' + name + '@' + version);

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(Notifications);
Vue.use(Vue2Touch);

Vue.prototype.$icons = icons;

Vue.component('fa-icon', FontAwesomeIcon);
Vue.component('Navigation', Navigation);
Vue.component('Facets', Facets);

const vue = new Vue({
  router,
  store,
  render: h => h(App),
});
vue.$mount('#app');

async function updateStatusAgain() {
  try {
    await store.dispatch('updateStatus');
    setTimeout(updateStatusAgain, 5000);
  } catch (error) {
    debug(`${error.message}\n${error.stack}`);
    vue.$notify({
      type: 'error',
      title: 'Error while receiving backend status',
      text: error.message,
    });
    setTimeout(updateStatusAgain, 60000);
  }
}

updateStatusAgain();
