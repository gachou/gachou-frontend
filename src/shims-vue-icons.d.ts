import Vue from 'vue';
import { icons } from './icons';
import { Store } from 'vuex';
import { IFrontendState } from '@/store';

declare module 'vue/types/vue' {
  interface Vue {
    $icons: typeof icons;
  }
}
