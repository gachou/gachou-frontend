Running unit-tests

```
npm run test:unit
```

In Webstorm/IDEA: Run configuration "test:unit" is checked in.
It uses the file `tests/unit/mocha-webpack.opts` for running mocha,
and the `mocha-webpack`-package.

# Frameworks

## Icons

* https://fontawesome.com/
* https://fontawesome.com/how-to-use/on-the-web/using-with/vuejs
* Icons are globally included in [main.ts](./src/main.ts)

## Styling / Components

* https://bootstrap-vue.js.org/
* 

## File Upload

* https://rowanwins.github.io/vue-dropzone/docs/dist/index.html#/installation