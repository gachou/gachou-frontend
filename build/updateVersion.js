const { name, version } = require('../package.json')
const fs = require('fs')

fs.writeFileSync(
  'src/version.ts',
  `export const name: string = '${name}'
export const version: string = '${version}'
`
)
